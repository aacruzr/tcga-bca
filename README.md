# TCGA-BCa annotations

This is a repository to store ground-truth region annotations from different pathologists into invasive BCa whole-slide images from The Cancer Genome Atlas (TCGA).

## XML format
The annotations were done by ImageScope viewer from Aperio over digitalized images from Aperio Scanner (.svs format file).

## Pathologists
* HG 
* MF
* NS

## Institutional data cohorts
* CINJ 
* TCGA

